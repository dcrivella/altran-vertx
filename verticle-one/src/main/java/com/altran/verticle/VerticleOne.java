package com.altran.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

public class VerticleOne extends AbstractVerticle {

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		
		vertx.createHttpServer().requestHandler(req -> {
			req.response()
			.putHeader("content-type", "application/json")
			.end("Hello from Vert.x!");
		}).listen(8888, http -> {
			if (http.succeeded()) {
				startFuture.complete();
				System.out.println("HTTP server started on port 8888");
			} else {
				startFuture.fail(http.cause());
			}
		});
	}
	
}
