package com.altran;

import com.altran.verticle.VerticleOne;

import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class VertxApplication {
	
	private static final Logger log = LoggerFactory.getLogger(VertxApplication.class);

	public static void main(String[] args) {
		
		Vertx vertx = Vertx.vertx();
		
		//vertx.deployVerticle(VerticleOne.class.getName());

		vertx.deployVerticle(new VerticleOne(), res -> {
            if (res.succeeded()) {
                log.info("Deployment ID is {} ", res.result());
            } else {
                log.error("Deployment failed!");
            }
        });
		
	}

}